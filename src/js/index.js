// import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
// import React from 'react';
// import ReactDOM from 'react-dom';
// require('./../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');

const deepCopy = require('deep-copy');

require('../css/styles.css');

const pluginPrefix = 'nodup';

// const pluginPath = 'http://localhost:8080/minerva-plugins/nodup/';
// const dupServiceUrl = 'http://localhost:8090/getPredictionForMinervaInstanceCached';
const dupServiceUrl = 'https://vibine.list.lu/getPredictionForMinervaInstanceCached';
const idPluginContainer = pluginPrefix + 'Container';
const idPluginSplitBar = 'pluginSplitBarDiv';
const idDuplicationContainer  = 'duplicationContainer';
const idDeduplicationContainer  = 'deduplicationContainer';
const idDupSettingsDiv = 'divDupSettings';
const idDdupSettingsDiv  = 'divDdupSettings';
const idRetrieveSuggestionsBtn = 'btnGetSuggestions';
const idOptionsBtn = 'btnOptions';
const idResultsDiv = 'divResults';
const idOptionsDiv = 'divOptions';
const cntResultsToShow = 30;

const defaultOnTypes = ['antisense rna', 'complex', 'degraded', 'element', 'gene', 'protein', 'phenotype', 'protein', 'rna', 'species, unknown'];

let euclideanInfo;
let retrieved = false;

let minervaProxy;
let pluginContainer;

let dDupGroups = {};

let $ = window.$;
if ($ === undefined && minerva.$ !== undefined) {
    $ = minerva.$;
}

// ******************************************************************************
// ********************* BEGIN MINERVA PLUGIN SPECIFICATION *********************
// ******************************************************************************

const register = function(_minerva) {
    minervaProxy = _minerva;
    pluginContainer = minervaProxy.element;
    console.log("registering plugin", minervaProxy);
    // includeCSS(pluginPath + 'dist/styles.css');
    $('div[name="minerva-plugin-div"]').width('500px'); //TODO replace with minerva function call when available
    return initPageStructure();
};

const unregister = function () {
    console.log("unregistering test plugin");
};

const getName = function() {
    return "Node (de)duplication";
};

const getVersion = function() {
    return "0.0.1";
};

minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion,
        minWidth: 400
    }
});

// ****************************************************************************
// ********************* END MINERVA PLUGIN SPECIFICATION *********************
// ****************************************************************************

function initPageStructure() {
    // $("#" + minervaAppDiv).css("overflow", "hidden");
    // $("#" + minervaAppDiv).css("width", "");
    //
    // $("#" + minervaAppDiv).before('<div id="' + idPluginContainer + '"></div>');

    // $(divPlugin).css("overflow", "hidden");
    // $(divPlugin).css("width", "");
    //
    // $(divPlugin).before('<div id="' + idPluginContainer + '"></div>');
    //
    // $("#" + idPluginContainer).after('<div id="' + idPluginSplitBar + '"></div>');
    //
    // $('#' + idPluginSplitBar).mousedown(function (e) {
    //     e.preventDefault();
    //     $(document).mousemove(function (e) {
    //         e.preventDefault();
    //         var x = $("body").width() - e.pageX;
    //         $('#' + idPluginContainer).css("width", x);
    //     })
    // });
    // $(document).mouseup(function (e) {
    //     $(document).unbind('mousemove');
    // });

    pluginContainer.innerHTML = `
        <div id="${idPluginContainer}">        
            <div class="container-fluid"> 
                <div class="row" id="divTopRow">
                    <div class="col-md-11">
                        <div id="divGetSuggestions">
                            <button id="${idRetrieveSuggestionsBtn}" type="button" class="btn btn-primary btn-block">
                                ${retrieveButtonHtml(false)}
                            </button>
                        </div>
                    </div>
                    <div class="col-md-1">                        
                        <button id="${idOptionsBtn}" type="button" class="btn btn-primary btn-block">
                            <i class="fa fa-bars"></i>
                        </button>                        
                    </div>                    
                </div>
            </div>
            <div id="${idOptionsDiv}" class="options" style="display: none">
                <div class="container-fluid">
                    <div class="row">              
                        <div class="euclidean col-md-12">
                            <label>
                                <input type="checkbox" checked> Euclidean distances
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel card border-success panel-success">
                                <div class="panel-heading card-header bg-success text-light">Duplication types</div>
                                <div id="${idDupSettingsDiv}" class="panel-body card-body">           
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel card border-danger panel-danger">
                                <div class="panel-heading card-header bg-danger text-light">Deduplication types</div>
                                <div id="${idDdupSettingsDiv}" class="panel-body card-body">           
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>                  
            </div>            
            <div id=${idResultsDiv} style="display: none">
                <div class="container-fluid"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-success card border-success">
                            <div class="panel-heading card-header bg-success text-light">Duplication suggestions</div>
                            <div id="${idDuplicationContainer}" class="panel-body card-body">           
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-danger card border-danger">
                            <div class="panel-heading  bg-danger text-light">Deduplication suggestions</div>
                            <div id="${idDeduplicationContainer}" class="panel-body card-body">           
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="container-fluid footer" style="display: none"> 
                <div class="row">
                    <div class="col-md-12">                        
                            <button class="btn btn-primary btn-block nodup-export-suggestions" type="button">
                                ${exportButtonHtml(false)}
                            </button>
                    </div>        
                </div>
            </div>
        </div>
        `;

    $('#' + idPluginContainer + ' #' + idRetrieveSuggestionsBtn).on("click", suggest);
    $('#' + idPluginContainer + ' .nodup-export-suggestions').on("click", exportSuggestions);
    $('#' + idPluginContainer + ' #' + idOptionsBtn).on("click", toggleOptions);
    $('#' + idPluginContainer + '.options .euclidean input[type="checkbox"]').on("click", udpateEuclidianInfo);
    udpateEuclidianInfo(); //sets euclideanInfo to the current value of the checkbox

    //remove focus off buttons after a click (without this it stays until the user click elsewhere)
    $('#' + idPluginContainer + ' .btn').mouseup( function(){ $(this).blur() });

    try {
        const entityTypes = [...new Set( minervaProxy.configuration.elementTypes.map(t => t.name))].sort();
        [idDupSettingsDiv, idDdupSettingsDiv].forEach(idC => {
            const container = $('#' + idPluginContainer + ' #' + idC)
            entityTypes.forEach(et => {
                const checked = defaultOnTypes.indexOf(et.toLocaleLowerCase()) >= 0 ? 'checked' : '';
                container.append('<div class="checkbox"><label><input type="checkbox" ' + checked + ' value="' + et + '">' + et + '</label></div>');
            })
        })
    } catch(err){
        console.log('err', err);
    }
}

function delay(t) {
    return new Promise(function(resolve) {
        setTimeout(resolve, t)
    });
}

function ajaxQuery(url, type) {
    if (type === undefined) type = "GET";

    return delay(0).then(function () {
        return $.ajax({
            type: type,
            url: url,
            dataType: 'json',
            crossDomain: true,
            contentType: "application/json; charset=UTF-8",
            // xhrFields: {
            //     withCredentials: true
            // }

        });
    });
}

function retrievePredictions(){
    // const address = 'http://localhost:8080/getPredictionForMinervaInstanceCached?instance_url=http://localhost:8080/minerva&project_id=PD_150625_3_pdba&model_id=879';
    // const address = pluginPath + '/data/pdmap_prediction.json';
    const address = dupServiceUrl +
        '?instance_url=' + ServerConnector.getServerBaseUrl().replace(/\/$/, '') +
        '&project_id=' + minervaProxy.project.data.getProjectId() +
        '&model_id=' + minervaProxy.project.data.getModels()[0].modelId +
        '&euclidean_info=' + (euclideanInfo ? 'T' : 'F');


    // console.log(address);
    return ajaxQuery(address);
}

function fetchBioObjects(ids) {

    let promise;
    const bioEntityIds = ids.map(id => { return {id: id, modelId: minervaProxy.project.data.getModels()[0].modelId, type:"ALIAS"} });

    let bioEntities;

    return minervaProxy.project.data.getBioEntityById(bioEntityIds).then(function (_bioEntities) {
        bioEntities = _bioEntities;
        const comparmentIds = bioEntities.map(bioEntity => {
            const compartmentId = bioEntity.getCompartmentId();
            return compartmentId ? compartmentId : -1; //this will cause to return undefined by getBioEntityById for entities with no compartment

        } );
        return minervaProxy.project.data.getBioEntityById(comparmentIds.map(id => { return {id: id, modelId: minervaProxy.project.data.getModels()[0].modelId, type:"ALIAS"} }));
    }).then(function (compartments) {
        compartments.forEach((comp, ix) =>  {
            bioEntities[ix].compartment = (comp ? comp : '');
        });
        return Promise.resolve(bioEntities);
    }).catch(function(d){
        console.log("Caught error when fetching bioentities", d, bioEntityIds);
    });
}

function convertToInnerJson(json) {
    // json = json[0];
    const converted = [];
    dDupGroups = {};
    for (let i =0; i < json.id.length; i++) {
        const groupId = json.group[i];
        if (!(groupId in dDupGroups)) dDupGroups[groupId]  = [];
        dDupGroups[groupId].push(json.id[i]);
        converted.push({
            actualDuplicate: json.actualDuplicate[i],
            deduplicationScore: json.deduplicationScore[i],
            duplicationScore: json.duplicationScore[i],
            id: json.id[i],
            predictedDuplicate: json.predictedDuplicate[i],
            type: json.type[i],
            groupId: json.group[i]
        })
    }
    return converted;
}

function toggleOptions(complete){
    $('#divOptions').slideToggle({duration: 'fast', complete: complete});
}

function udpateEuclidianInfo(){
    euclideanInfo =  $('.options .euclidean input[type="checkbox"]')[0].checked;
}

function dehighlightAllBioentities(filter){
    return minervaProxy.project.map.getHighlightedBioEntities().then(function(highlighted) {
        if (typeof filter === 'string') highlighted = highlighted.filter(h => h.type === filter);
        else if (typeof filter === 'object' && filter.constructor.name === 'Alias') highlighted = highlighted.filter(h => h.type === 'SURFACE' && h.element.id === filter.id);
        return minervaProxy.project.map.hideBioEntity(highlighted);
    });

}

function exportSuggestions(){

    const self = this;
    function exportButtonState(activate) {
        $(self).html(exportButtonHtml(activate))
    }


    exportButtonState(true);

    const dupElementIds = $(`#${idPluginContainer} #${idDuplicationContainer} input:checkbox:checked`).map(function (ix, el) {
        //parent of checkbox = td; parent of td = tr -> data -> bioentity -> id of element
        return $(el).parent().parent().data().predJson.bioEntity.getElementId();
    });
    const ddupElementIds = $(`#${idPluginContainer} #${idDeduplicationContainer} input:checkbox:checked`).map(function (ix, el) {
        return $(el).parent().parent().data().predJson.bioEntity.getElementId();
    });

    let overlayContent = 'elementIdentifier\tcolor\n';
    let  color = getColor(true, 1);
    dupElementIds.each(function(ix, id) {
        overlayContent += `${id}\t${color}\n`
    });
    color = getColor(false, 1);
    ddupElementIds.each(function(ix, id) {
        overlayContent += `${id}\t${color}\n`
    });

    let createdOverlay;
    return ServerConnector.addOverlayFromString('tempDedupOverlay', overlayContent).then(function (overlay) {
        createdOverlay = overlay;
        return ServerConnector.getModelConverters();
    }).then(function (converters) {
        let ixConverter;

        for (let i = 0; i< converters.length; i++) {
            if (converters[i].name === "CellDesigner SBML") {
                ixConverter = i;
            }
        }
        return ServerConnector.getModelDownloadUrl({
            modelId: minervaProxy.project.data.getModels()[0].modelId,
            handlerClass: converters[ixConverter].handler,
            // backgroundOverlayId: map.getGoogleMap().getMapTypeId(),
            // zoomLevel: map.getGoogleMap().getZoom(),
            overlayIds: [createdOverlay.id]
        });
    }).then(function (url) {
        exportButtonState(false);
        const win = window.open(url, "_blank");
        win.addEventListener('unload', function () {
            return ServerConnector.removeOverlay({overlayId: createdOverlay.id});
        })
    }).catch(function (err) {
        console.log('err',err);
        exportButtonState(false);
        errorMessage('You need to be logged in to export a map with color overlay.')
        if (createdOverlay) ServerConnector.removeOverlay({overlayId: createdOverlay.id});
    });
}

function getColor(duplication, score) {

    function rgb2hex(red, green, blue) {
        const rgb = blue | (green << 8) | (red << 16);
        return '#' + (0x1000000 + rgb).toString(16).slice(1)
    }

    return duplication ? rgb2hex(0, 255, 0) : rgb2hex(255, 0, 0)
}

function getHighlightDef(bioEntity, duplication, score) {
    return {
        element: {
            id: bioEntity.id,
                modelId: minervaProxy.project.data.getModels()[0].modelId,
                type: "ALIAS"
        },
        type: "SURFACE",
        options: {
            // color: getColorForElement($(this)[0])
            color: getColor(duplication, score),
            opacity: 0.5
        }
    }
}

function highlight(highlightDefs) {
    return minervaProxy.project.map.showBioEntity(highlightDefs);

}

function errorMessage(message){
    const messageMarkup = `
            <div class="col-md-12 pt-2">
                <div class="panel panel-danger card border-danger  nodup-error">
                    <div class="panel-heading card-header bg-danger text-light">Error</div>
                    <div class="panel-body card-body">
                        ${message}
                    </div>
                </div>
            </div>
        `;
    $("#" + idResultsDiv).before(messageMarkup);
    $('#'+  idRetrieveSuggestionsBtn).html(retrieveButtonHtml(false));
}

function removeErrorMessages(){
    $('.nodup-error').remove();

}

function suggest() {
    removeErrorMessages();
    const suggestAfterTooggle = function () {
        $('#'+  idRetrieveSuggestionsBtn).html(retrieveButtonHtml(true));

        retrievePredictions().then(function (predJson) {


            // console.log('original prediction json', predJson);
            predJson = convertToInnerJson(predJson);
            // console.log('converted json', predJson);

            const typesDup = [], typesDdup = [];
            $('#' + idPluginContainer + ' #' + idDupSettingsDiv + ' input:checkbox:checked').each(function(i, val){typesDup.push(val.value.toLowerCase())});
            $('#' + idPluginContainer + ' #' + idDdupSettingsDiv + ' input:checkbox:checked').each(function(i, val){typesDdup.push(val.value.toLowerCase())});


            //Create copies, filter out unwanted entity types and sort results for duplication and deduplication
            let predJsonDup = deepCopy(predJson).filter(pred => typesDup.indexOf(pred.type.toLowerCase()) >= 0).sort(function(a, b) {
                return b.duplicationScore - a.duplicationScore;
            });
            let predJsonDdup = deepCopy(predJson).filter(pred => pred.actualDuplicate && typesDdup.indexOf(pred.type.toLowerCase()) >= 0).sort(function(a, b) {
                return b.deduplicationScore - a.deduplicationScore;
            });

            //Keep only top cntResultsToShow results
            predJsonDup.splice(cntResultsToShow);
            predJsonDdup.splice(cntResultsToShow);

            // console.log('predJsonDup', predJsonDup);
            // console.log('predJsonDdup', predJsonDdup);

            //Fetch bioEntities for the ids of both dup and ddup results and join them to the json arrays
            return fetchBioObjects(predJsonDup.map(pred => pred.id)).then(function (bioEntities){
                predJsonDup.forEach((pred, ix) => {pred.bioEntity = bioEntities[ix]; return pred;});
                return fetchBioObjects(predJsonDdup.map(pred => pred.id));
            }).then(function (bioEntities) {

                predJsonDdup.forEach((pred, ix) => {pred.bioEntity = bioEntities[ix]; return pred;});

                for (let i =0; i < predJsonDup.length; i++) {
                    if (!predJsonDup[i].bioEntity) {
                        return Promise.reject("One of the DUP IDs returned from Ddup service could not be mapped to bioentity.")
                    }
                }
                for (let i =0; i < predJsonDdup.length; i++) {
                    if (!predJsonDdup[i].bioEntity) {
                        return Promise.reject("One of the DDUP IDs returned from Ddup service could not be mapped to bioentity.")
                    }
                }

                //Highlight the corresponding entities in the map

                return dehighlightAllBioentities().then(function(){
                    // const highlightedIds = highlighted.map(function(h){return h.element.id});
                    const highligtDefs = {};
                    predJsonDup.forEach(pred =>  highligtDefs[pred.bioEntity.id] = getHighlightDef(pred.bioEntity, true, pred.duplicationScore));
                    predJsonDdup.forEach(pred =>  highligtDefs[pred.bioEntity.id] = getHighlightDef(pred.bioEntity, false, pred.deduplicationScore));
                    highlight(Object.keys(highligtDefs).map(k => highligtDefs[k]));

                    function createTable(predJson, scoreType, dupContainer) {
                        //Create tables with the (de)duplication suggestions
                        $('#' + idPluginContainer + ' #' + dupContainer).html(
                            `<table class="table table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Compartment</th>
                            <th>Type</th>
                            <th>Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        ${predJson.map(pred =>
                                `<tr>
                                <td><input id="checkBox" type="checkbox" checked></td>
                                <td>${pred.bioEntity.getName()}</td>
                                <td>${pred.bioEntity.compartment ? pred.bioEntity.compartment.getName() : ''}</td>
                                <td>${pred.bioEntity.getType()}</td>
                                <td>${pred[scoreType]}</td>
                             </tr>`).join('')
                                }
                        </tbody>
                    </table>`
                        );

                        const table = $('#' + idPluginContainer + ' #' + dupContainer + ' table');
                        table.find("th").each(function(ix){
                            $(this).on('click', () => sortTable(table[0], ix));
                        });

                        $('#' + idPluginContainer + ' #' + dupContainer + ' tbody tr').each((i,tr) => $.data(tr, 'predJson', predJson[i]));
                    }

                    createTable(predJsonDup, 'duplicationScore', idDuplicationContainer);
                    createTable(predJsonDdup, 'deduplicationScore', idDeduplicationContainer);

                    //Add onclick events using the bioEntities attached to the rows
                    $('#' + idPluginContainer + ' #' + idDuplicationContainer + ' tbody tr').on('click', function () {
                        const pred = $.data(this, 'predJson');
                        const rowChecked = $(this).find('input')[0].checked;
                        return dehighlightAllBioentities(pred.bioEntity).then(function(){
                            if (rowChecked) highlight(getHighlightDef(pred.bioEntity, true, pred.duplicationScore));

                            minervaProxy.project.map.fitBounds({
                                modelId: minervaProxy.project.data.getModels()[0].modelId,
                                x1: pred.bioEntity.getX(),
                                y1: pred.bioEntity.getY(),
                                x2: pred.bioEntity.getX() + pred.bioEntity.getWidth(),
                                y2: pred.bioEntity.getY() + pred.bioEntity.getHeight()
                            });
                        })
                    });
                    $('#' + idPluginContainer + ' #' + idDeduplicationContainer + ' tbody tr').on('click', function () {
                        const rowChecked = $(this).find('input')[0].checked;
                        const pred = $.data(this, 'predJson');
                        const groupIds = dDupGroups[pred.groupId];
                        //use icons to highlight all elements in the deduplication suggestion group
                        return dehighlightAllBioentities("ICON").then(function() {
                            return fetchBioObjects(groupIds);
                        }).then( function (bioEntities) {
                            (function() {
                                if (rowChecked) {
                                    return dehighlightAllBioentities(pred.bioEntity).then(function() {

                                            const highlightDefs = bioEntities.map(be => {
                                                return {
                                                    element: {
                                                        id: be.id,
                                                        modelId: minervaProxy.project.data.getModels()[0].modelId,
                                                        type: "ALIAS"
                                                    },
                                                    type: "ICON"
                                                }
                                            });
                                            return highlight(highlightDefs.concat([getHighlightDef(pred.bioEntity, false, pred.deduplicationScore)]));

                                    });
                                } else {
                                    return dehighlightAllBioentities(pred.bioEntity)
                                    // return Promise.resolve();
                                }
                            })().then(function(){
                                return minervaProxy.project.map.fitBounds({
                                    modelId: minervaProxy.project.data.getModels()[0].modelId,
                                    x1: pred.bioEntity.getX(),
                                    y1: pred.bioEntity.getY(),
                                    x2: pred.bioEntity.getX() + pred.bioEntity.getWidth(),
                                    y2: pred.bioEntity.getY() + pred.bioEntity.getHeight()
                                });
                            });
                        });
                    });

                    retrieved = true;
                    $('#' + idPluginContainer + ' #' + idResultsDiv).css('display', 'block');
                    const resultsPositionTop = $('#' + idPluginContainer + ' #' + idResultsDiv + '>div').offset().top;
                    const footer = $('#' + idPluginContainer + ' .footer');
                    footer.css('display', 'block');
                    $('#' + idPluginContainer + ' #' + idResultsDiv + ' .row').css('height', `calc( ( 100vh - 10px - ${resultsPositionTop}px - ${footer.height()}px ) / 2 )`);
                    $('#' + idPluginContainer + ' #'+  idRetrieveSuggestionsBtn).html(retrieveButtonHtml(false));

                    return Promise.resolve();

                });

            });
        }).catch(function (err){
            let message = '';
            if (typeof err === 'object' && status in err) {
                message = '<div>Duplication service responded with error code ' + err.status + '</div>';
            } else {
                message = '<div>Error when retrieving the suggestions.</div>' +
                    '<ul><li>Please verify that the service at <a herf="https://vibine.list.lu/getStatus" target="_blank">https://vibine.list.lu</a> can be accessed.</li>' +
                    '<li>If the service is accessible, find error detail in the developers console of your browser.</li></ul>';
                console.log(err);
            }
            errorMessage(message);
        });
    };

    if ($('#divOptions').css('display') == 'block') toggleOptions(suggestAfterTooggle);
    else suggestAfterTooggle();
}

function retrieveButtonHtml(inProgress) {
    return inProgress ?
        '<i class="fa fa-circle-o-notch fa-spin"></i> Retrieving suggestions' :
        'Retrieve (de)duplication suggestions';
}

function exportButtonHtml(inProgress){
    return inProgress ?
        '<i class="fa fa-circle-o-notch fa-spin"></i> Exporting' :
        'Export (with login only)';

}

function sortTable(table, n) {
    var rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
     no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /* Loop through all table rows (except the
         first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
             one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
             based on the direction, asc or desc: */
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
             and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else {
            /* If no switching has been done AND the direction is "asc",
             set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}


// module.exports = nodup;
