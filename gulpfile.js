var gulp = require('gulp');

var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
// var uglify = require('gulp-uglify');
var sass = require('gulp-sass');


gulp.task('browserify', function() {
    return  browserify({
        entries: ['./src/js/index.js'],
        transform: [
            ['babelify', {
                "presets": ['es2015'],
                "compact": false
            }
            ],
            ['browserify-css']
        ],
        debug: true
    }).require("./src/js/index.js", {expose: "nodup"}).on('error', e => console.log(e))
        .bundle().on('error', e => console.log(e))
        .pipe(source('plugin.js'))
        .pipe(buffer())
        // .pipe(uglify())
        .pipe(gulp.dest('dist'));
});


gulp.task('css', function() {
    return gulp.src('src/css/styles.scss')
        .pipe(sass().on('error', sass.logError))
        // .pipe(gulp.dest('dist'));
});

gulp.task('default', gulp.series(['css', 'browserify' ]));

gulp.task('watch', gulp.series(['default' ], function(){
    gulp.watch('src/**/*', ['css', 'browserify']);
}));

