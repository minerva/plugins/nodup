# Node duplication plugin

A plugin which, based on a pretrained model, suggest nodes which would
be candidates for potential duplication or deduplication.

### General instructions

In order to use precompiled and publicly available version of the plugin, 
open the plugin menu in the MINERVA's upper left corner and click plugins.
In the dialog which appears enter the following address in the URL box: `https://minerva-dev.lcsb.uni.lu/plugins/nodup/plugin.js` .
The plugin shows up in the plugins panel on the right hand side of the screen.

The plugin can be tested with the [Parkinson's disease map](https://pdmap.uni.lu/minerva/#), but
can be run against any other network on any MINERVA instance supporting
plugins.



The plugin relies on an external service running at [https://vibine.list.lu](https://vibine.list.lu).
The availability of the service can be tested by accessing
[https://vibine.list.lu/getStatus](https://vibine.list.lu/getStatus).
The first query on a map can take a lot of time since the service first downloads
the whole map, computes various graph-based features for every species
in the map and only then returns the predictions by querying a pretrained model.
The features are then cached.


### Building the plugin

To compile the plugin run:

- ```npm install```
- ```npm run build```

After that, you should see ```plugin.js``` file in the ```dist``` directory. 
This file needs to be uploaded to a place where your Minerva instance can see it.
Then you simply use the URI of the ```plugin.js``` in the Minerva's plugin
interface to load it.